/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ph_take_1.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 21:32:40 by pmarquis          #+#    #+#             */
/*   Updated: 2023/02/21 21:49:22 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ph_take_1(t_parms *p, t_phil *ph)
{
	if (!wait_fork(p, ph, ph->fork1, 0))
		return (0);
	fork_mtx_lock(p, ph->fork1);
	prnt_has_taken_fork(p, ph);
	return (1);
}
