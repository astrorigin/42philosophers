/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ph_sleep.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 22:06:50 by pmarquis          #+#    #+#             */
/*   Updated: 2023/02/21 18:40:24 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ph_sleep(t_parms *p, t_phil *ph)
{
	size_t	sleep_t;

	if (stopped(p))
		return (0);
	if (is_dead(p, ph))
		return (ph_die(p, ph, 0));
	prnt_is_sleeping(p, ph);
	sleep_t = ph->lastmeal + (p->tteat * 1000) + (p->ttslp * 1000);
	while (1)
	{
		if (stopped(p))
			return (0);
		if (is_dead(p, ph))
			return (ph_die(p, ph, 0));
		if (now_usec() >= sleep_t)
			break ;
	}
	return (1);
}
