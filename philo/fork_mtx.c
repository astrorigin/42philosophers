/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork_mtx.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/21 21:19:07 by pmarquis          #+#    #+#             */
/*   Updated: 2023/02/21 22:16:21 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	fork_mtx_lock(t_parms *p, size_t nfork)
{
	t_fork	*f;

	f = &p->forks[nfork];
	mtx_lock(&f->mtx);
	mtx_lock(&f->ready_mtx);
	f->ready = 1;
	mtx_unlock(&f->ready_mtx);
}

void	fork_mtx_unlock(t_parms *p, size_t nfork)
{
	t_fork	*f;

	f = &p->forks[nfork];
	mtx_unlock(&f->mtx);
	mtx_lock(&f->ready_mtx);
	f->ready = 0;
	mtx_unlock(&f->ready_mtx);
}
