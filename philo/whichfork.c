/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   whichfork.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 16:51:33 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/02 21:10:56 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

size_t	next_(size_t fork, size_t nforks)
{
	if (fork == nforks)
		return (0);
	return (fork);
}

/*
 *	Determine which fork to take first
 */
size_t	which_fork_1(size_t philnum, size_t nphil)
{
	if (philnum % 2 == 1)
		return (next_(philnum, nphil));
	return (philnum - 1);
}

/*
 *	Determine which fork to take second
 */
size_t	which_fork_2(size_t philnum, size_t nphil)
{
	if (philnum % 2 == 1)
		return (philnum - 1);
	return (next_(philnum, nphil));
}
