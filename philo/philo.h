/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/04 20:52:11 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/02 23:24:05 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <assert.h>
# include <pthread.h>
# include <unistd.h>
# include <sys/time.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

typedef struct s_parms	t_parms;

typedef struct s_phil
{
	size_t			num;
	size_t			fork1;
	size_t			fork2;
	size_t			lastmeal;
	size_t			nmeals;
	int				perm;
	pthread_mutex_t	perm_mtx;
	pthread_t		tid;
	t_parms			*parms;
}	t_phil;

typedef struct s_fork
{
	pthread_mutex_t	mtx;
	int				ready;
	pthread_mutex_t	ready_mtx;
}	t_fork;

struct s_parms
{
	size_t			nphil;
	size_t			ttdie;
	size_t			tteat;
	size_t			ttslp;
	size_t			nmeat;
	t_phil			*phils;
	t_fork			*forks;
	int				stop;
	pthread_mutex_t	stop_mtx;
};

int		error(const char *msg);

int		fini(t_parms *p);
int		init(t_parms *p);

size_t	next_(size_t fork, size_t nforks);
size_t	which_fork_1(size_t philnum, size_t nphil);
size_t	which_fork_2(size_t philnum, size_t nphil);

void	prnt_is_thinking(t_parms *p, const t_phil *ph);
void	prnt_has_taken_fork(t_parms *p, const t_phil *ph);
void	prnt_is_eating(t_parms *p, const t_phil *ph);
void	prnt_is_sleeping(t_parms *p, const t_phil *ph);
void	prnt_died(t_parms *p, const t_phil *ph);

size_t	now_msec(void);
size_t	now_usec(void);

void	mtx_lock(pthread_mutex_t *mtx);
void	mtx_unlock(pthread_mutex_t *mtx);

void	fork_mtx_lock(t_parms *p, size_t nfork);
void	fork_mtx_unlock(t_parms *p, size_t nfork);

void	*start(void *param);
int		is_dead(const t_parms *p, const t_phil *ph);
int		stopnow(t_parms *p);
int		stopped(t_parms *p);

int		wait_till(t_parms *p, t_phil *ph, size_t t, size_t n);
int		wait_perm(t_parms *p, t_phil *ph);
int		wait_fork(t_parms *p, t_phil *ph, size_t nfork, size_t n);

int		ph_die(t_parms *p, t_phil *ph, int unlock);
int		ph_think(t_parms *p, t_phil *ph);
int		ph_take_1(t_parms *p, t_phil *ph);
int		ph_take_2(t_parms *p, t_phil *ph);
int		ph_eat(t_parms *p, t_phil *ph);
int		ph_sleep(t_parms *p, t_phil *ph);

// libft

typedef unsigned long	t_ulong;

char	*ft_atosz(const char *s, size_t *ret);
int		ft_atosz_ex(const char *s, size_t *ret, int spec);
void	*ft_calloc(size_t nmemb, size_t size);
int		ft_isdigit(int c);
int		ft_isspace(int c);
t_ulong	ft_ulmin(t_ulong a, t_ulong b);
int		ft_multovf(size_t a, size_t b);
int		ft_putstr(const char *s, int fd);
char	*ft_strchr(const char *s, char c);
size_t	ft_strlen(const char *s);

#endif
