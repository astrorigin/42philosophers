/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wait.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/21 20:49:35 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/02 23:09:08 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	wait_till(t_parms *p, t_phil *ph, size_t t, size_t n)
{
	while (1)
	{
		if (stopped(p))
			return (0);
		if (is_dead(p, ph))
			return (ph_die(p, ph, n));
		if (now_usec() >= t)
			return (1);
	}
}

int	wait_perm(t_parms *p, t_phil *ph)
{
	int	perm;

	while (1)
	{
		if (stopped(p))
		{
			fork_mtx_unlock(p, ph->fork1);
			return (0);
		}
		if (is_dead(p, ph))
			return (ph_die(p, ph, 1));
		mtx_lock(&ph->perm_mtx);
		perm = ph->perm;
		mtx_unlock(&ph->perm_mtx);
		if (perm)
		{
			mtx_lock(&ph->perm_mtx);
			ph->perm = 0;
			mtx_unlock(&ph->perm_mtx);
			return (1);
		}
	}
}

static int	_fork_ready(t_parms *p, size_t nfork)
{
	int	i;

	mtx_lock(&p->forks[nfork].ready_mtx);
	i = p->forks[nfork].ready;
	mtx_unlock(&p->forks[nfork].ready_mtx);
	if (i == 0)
		return (1);
	return (0);
}

int	wait_fork(t_parms *p, t_phil *ph, size_t nfork, size_t n)
{
	while (1)
	{
		if (stopped(p))
		{
			if (n)
				fork_mtx_unlock(p, ph->fork1);
			if (n > 1)
				fork_mtx_unlock(p, ph->fork2);
			return (0);
		}
		if (is_dead(p, ph))
			return (ph_die(p, ph, n));
		if (_fork_ready(p, nfork))
			return (1);
	}
}
