/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mtx.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 20:41:47 by pmarquis          #+#    #+#             */
/*   Updated: 2023/02/21 21:27:18 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	mtx_lock(pthread_mutex_t *mtx)
{
	if (pthread_mutex_lock(mtx))
		error("pthread_mutex_lock\n");
}

void	mtx_unlock(pthread_mutex_t *mtx)
{
	if (pthread_mutex_unlock(mtx))
		error("pthread_mutex_unlock\n");
}
