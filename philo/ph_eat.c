/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ph_eat.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 21:49:15 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/03 00:37:08 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static int	_check_nmeals(t_parms *p, t_phil *ph)
{
	size_t	slp_t;

	if (p->nmeat && ph->nmeals == p->nmeat)
	{
		prnt_is_sleeping(p, ph);
		slp_t = ph->lastmeal + (p->tteat * 1000) + (p->ttslp * 1000);
		while (1)
		{
			if (stopped(p))
				return (1);
			if (now_usec() >= slp_t)
				break ;
		}
		prnt_is_thinking(p, ph);
		return (1);
	}
	return (0);
}

static int	_eat(t_parms *p, t_phil *ph, size_t eat_t)
{
	while (1)
	{
		if (stopped(p))
		{
			fork_mtx_unlock(p, ph->fork1);
			fork_mtx_unlock(p, ph->fork2);
			return (0);
		}
		if (is_dead(p, ph))
			return (ph_die(p, ph, 2));
		if (now_usec() >= eat_t)
			return (1);
	}
}

static void	_give_perm(t_parms *p, t_phil *ph)
{
	const size_t	n = next_(ph->num, p->nphil);

	mtx_lock(&p->phils[n].perm_mtx);
	p->phils[n].perm = 1;
	mtx_unlock(&p->phils[n].perm_mtx);
}

int	ph_eat(t_parms *p, t_phil *ph)
{
	size_t	eat_t;

	if (stopped(p))
	{
		fork_mtx_unlock(p, ph->fork1);
		fork_mtx_unlock(p, ph->fork2);
		return (0);
	}
	ph->lastmeal = now_usec();
	ph->nmeals += 1;
	prnt_is_eating(p, ph);
	eat_t = ph->lastmeal + (p->tteat * 1000);
	if (!_eat(p, ph, eat_t))
		return (0);
	_give_perm(p, ph);
	fork_mtx_unlock(p, ph->fork1);
	fork_mtx_unlock(p, ph->fork2);
	if (_check_nmeals(p, ph))
		return (0);
	return (1);
}
