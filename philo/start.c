/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   start.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 15:56:01 by pmarquis          #+#    #+#             */
/*   Updated: 2023/02/21 22:05:44 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static void	_several_phil(t_parms *p, t_phil *ph)
{
	while (1)
	{
		if (!ph_think(p, ph) || !ph_take_1(p, ph) || !ph_take_2(p, ph)
			|| !ph_eat(p, ph) || !ph_sleep(p, ph))
			return ;
	}
}

static void	_unique_phil(t_parms *p, t_phil *ph)
{
	prnt_is_thinking(p, ph);
	fork_mtx_lock(p, 0);
	prnt_has_taken_fork(p, ph);
	usleep((p->ttdie * 1000) - (now_usec() - ph->lastmeal));
	prnt_died(p, ph);
}

void	*start(void *param)
{
	t_phil	*ph;
	t_parms	*p;

	ph = param;
	p = ph->parms;
	ph->lastmeal = now_usec();
	if (p->ttdie == 0)
		ph_die(p, ph, 0);
	else if (p->nphil == 1)
		_unique_phil(p, ph);
	else
		_several_phil(p, ph);
	return (0);
}
