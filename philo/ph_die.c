/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ph_die.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 19:53:36 by pmarquis          #+#    #+#             */
/*   Updated: 2023/02/21 21:30:55 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

//	let die...

int	ph_die(t_parms *p, t_phil *ph, int unlock)
{
	if (unlock)
	{
		fork_mtx_unlock(p, ph->fork1);
		if (unlock > 1)
			fork_mtx_unlock(p, ph->fork2);
	}
	if (stopped(p))
		return (0);
	stopnow(p);
	prnt_died(p, ph);
	return (0);
}
