/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stop.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/21 14:23:26 by pmarquis          #+#    #+#             */
/*   Updated: 2023/02/26 19:00:42 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	stopped(t_parms *p)
{
	int	i;

	mtx_lock(&p->stop_mtx);
	i = p->stop;
	mtx_unlock(&p->stop_mtx);
	return (i);
}

int	stopnow(t_parms *p)
{
	mtx_lock(&p->stop_mtx);
	p->stop = 1;
	mtx_unlock(&p->stop_mtx);
	return (0);
}
