/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   now.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 16:17:24 by pmarquis          #+#    #+#             */
/*   Updated: 2023/02/21 18:40:24 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static size_t	_tv2msec(const struct timeval *tv)
{
	return ((tv->tv_sec * 1000) + (tv->tv_usec / 1000));
}

static size_t	_tv2usec(const struct timeval *tv)
{
	return ((tv->tv_sec * 1000000) + tv->tv_usec);
}

size_t	now_msec(void)
{
	struct timeval	tv;

	gettimeofday(&tv, 0);
	return (_tv2msec(&tv));
}

size_t	now_usec(void)
{
	struct timeval	tv;

	gettimeofday(&tv, 0);
	return (_tv2usec(&tv));
}
