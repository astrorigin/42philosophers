/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/04 22:09:23 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/02 23:21:16 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	is_dead(const t_parms *p, const t_phil *ph)
{
	if (now_usec() >= (ph->lastmeal + (p->ttdie * 1000)))
		return (1);
	return (0);
}

static int	_initargs(t_parms *p, int ac, char *av[])
{
	if (ac < 5 || ac > 6)
		return (0);
	memset(p, 0, sizeof(t_parms));
	if (!ft_atosz_ex(av[1], &p->nphil, 1)
		|| !ft_atosz_ex(av[2], &p->ttdie, 0)
		|| !ft_atosz_ex(av[3], &p->tteat, 0)
		|| !ft_atosz_ex(av[4], &p->ttslp, 0))
		return (0);
	if (ac == 6 && !ft_atosz_ex(av[5], &p->nmeat, 0))
		return (0);
	return (1);
}

int	main(int argc, char *argv[])
{
	t_parms	p;
	size_t	i;

	if (!_initargs(&p, argc, argv) || !init(&p))
		return (1);
	i = -1;
	while (++i < p.nphil)
	{
		if (pthread_create(&p.phils[i].tid, 0, &start, &p.phils[i]))
			return (1 + fini(&p) + error("pthread_create\n"));
		usleep(30);
	}
	i = -1;
	while (++i < p.nphil)
	{
		if (pthread_join(p.phils[i].tid, 0))
			error("pthread_join\n");
	}
	return (fini(&p));
}
