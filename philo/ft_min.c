/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_min.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/20 03:10:38 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/08 14:19:42 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

t_ulong	ft_ulmin(t_ulong a, t_ulong b)
{
	if (a <= b)
		return (a);
	return (b);
}
