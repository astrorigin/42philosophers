/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 19:40:51 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/09 23:11:32 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

/*
 *	With 3 philos: all are "thiefs", none wait for "permission".
 *	Else, they alternate: one "thief", one "altruist" waiting for "perm", etc,
 *	but the last one who must "altruist".
 */

static int	_init_phils(t_parms *p)
{
	size_t	i;
	t_phil	*ph;

	i = -1;
	while (++i < p->nphil)
	{
		ph = &p->phils[i];
		ph->num = i + 1;
		if (p->nphil == 3)
		{
			ph->fork1 = next_(i + 1, p->nphil);
			ph->fork2 = i;
			ph->perm = 1;
		}
		else
		{
			ph->fork1 = which_fork_1(i + 1, p->nphil);
			ph->fork2 = which_fork_2(i + 1, p->nphil);
			ph->perm = (i % 2 == 0 && i != p->nphil - 1);
		}
		pthread_mutex_init(&ph->perm_mtx, 0);
		ph->parms = p;
	}
	return (1);
}

static int	_init_forks(t_parms *p)
{
	size_t	i;
	t_fork	*f;

	i = -1;
	while (++i < p->nphil)
	{
		f = &p->forks[i];
		pthread_mutex_init(&f->mtx, 0);
		pthread_mutex_init(&f->ready_mtx, 0);
	}
	return (1);
}

int	init(t_parms *p)
{
	p->phils = ft_calloc(p->nphil, sizeof(t_phil));
	if (!p->phils)
		return (0);
	p->forks = ft_calloc(p->nphil, sizeof(t_fork));
	if (!p->forks)
		return (0);
	if (!_init_phils(p) || !_init_forks(p))
		return (0);
	pthread_mutex_init(&p->stop_mtx, 0);
	return (1);
}

int	fini(t_parms *p)
{
	size_t	i;

	if (p->forks)
	{
		i = -1;
		while (++i < p->nphil)
		{
			pthread_mutex_destroy(&p->forks[i].mtx);
			pthread_mutex_destroy(&p->forks[i].ready_mtx);
		}
		free(p->forks);
		p->forks = 0;
	}
	if (p->phils)
	{
		i = -1;
		while (++i < p->nphil)
			pthread_mutex_destroy(&p->phils[i].perm_mtx);
		free(p->phils);
		p->phils = 0;
	}
	pthread_mutex_destroy(&p->stop_mtx);
	return (0);
}
