/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ph_think.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 21:27:49 by pmarquis          #+#    #+#             */
/*   Updated: 2023/02/21 18:37:08 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	ph_think(t_parms *p, t_phil *ph)
{
	if (stopped(p))
		return (0);
	if (is_dead(p, ph))
		return (ph_die(p, ph, 0));
	prnt_is_thinking(p, ph);
	return (1);
}
