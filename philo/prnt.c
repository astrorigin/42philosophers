/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   prnt.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 16:43:27 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/02 23:30:07 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

void	prnt_is_thinking(t_parms *p, const t_phil *ph)
{
	if (!stopped(p))
		printf("%zu %02zu is thinking\n", now_msec(), ph->num);
}

void	prnt_has_taken_fork(t_parms *p, const t_phil *ph)
{
	if (!stopped(p))
		printf("%zu %02zu has taken a fork\n", now_msec(), ph->num);
}

void	prnt_is_eating(t_parms *p, const t_phil *ph)
{
	if (!stopped(p))
		printf("%zu %02zu is eating (%zu)\n", now_msec(), ph->num, ph->nmeals);
}

void	prnt_is_sleeping(t_parms *p, const t_phil *ph)
{
	if (!stopped(p))
		printf("%zu %02zu is sleeping\n", now_msec(), ph->num);
}

void	prnt_died(t_parms *p, const t_phil *ph)
{
	(void) p;
	printf("%zu %02zu died\n", now_msec(), ph->num);
}
