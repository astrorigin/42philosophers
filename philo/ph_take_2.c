/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ph_take_2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/18 21:37:41 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/09 23:14:29 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

/*
 *	Here some must wait for permission.
 */

int	ph_take_2(t_parms *p, t_phil *ph)
{
	if ((p->nphil == 3 || ph->num % 2 == 1) && !wait_perm(p, ph))
		return (0);
	if (!wait_fork(p, ph, ph->fork2, 1))
		return (0);
	fork_mtx_lock(p, ph->fork2);
	prnt_has_taken_fork(p, ph);
	return (1);
}
