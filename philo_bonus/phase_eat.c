/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phase_eat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 04:50:37 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/10 15:12:41 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static int	_nirvana(t_phil *ph)
{
	ph->flag = 1;
	if (sem_wait(ph->parms->prnt_sem))
		return (error("sem_wait\n"));
	printf("%zu %02zu is sleeping\n", now_msec(), ph->num);
	if (sem_post(ph->parms->prnt_sem))
		return (error("sem_post\n"));
	usleep(ph->parms->ttslp * 1000);
	if (sem_wait(ph->parms->prnt_sem))
		return (error("sem_wait\n"));
	printf("%zu %02zu is thinking\n", now_msec(), ph->num);
	if (sem_post(ph->parms->prnt_sem))
		return (error("sem_post\n"));
	if (sem_post(ph->parms->stop_sem))
		return (error("sem_post\n"));
	return (0);
}

int	phase_eat(t_phil *ph)
{
	gettimeofday(&ph->tv, 0);
	ph->nmeals += 1;
	if (sem_wait(ph->parms->prnt_sem))
		return (error("sem_wait\n"));
	printf("%zu %02zu is eating (%zu)\n", tv2msec(&ph->tv), ph->num, ph->nmeals);
	if (sem_post(ph->parms->prnt_sem))
		return (error("sem_post\n"));
	usleep((ph->parms->tteat * 1000) - (now_usec() - tv2usec(&ph->tv)));
	if (sem_post(ph->parms->phils[next_(ph->num - 1, ph->parms->nphil)])
		|| sem_post(ph->parms->forks) || sem_post(ph->parms->forks))
		return (error("sem_post\n"));
	if (ph->nmeals == ph->parms->nmeat)
		return (_nirvana(ph));
	return (1);
}
