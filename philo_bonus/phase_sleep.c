/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phase_sleep.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 04:57:05 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/10 15:12:41 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	phase_sleep(t_phil *ph)
{
	if (sem_wait(ph->parms->prnt_sem))
		return (error("sem_wait\n"));
	printf("%zu %02zu is sleeping\n", now_msec(), ph->num);
	if (sem_post(ph->parms->prnt_sem))
		return (error("sem_post\n"));
	usleep(tv2usec(&ph->tv) + (ph->parms->tteat * 1000)
		+ (ph->parms->ttslp * 1000) - now_usec());
	return (1);
}
