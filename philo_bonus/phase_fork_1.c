/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phase_fork_1.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 04:42:26 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/10 15:12:41 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	phase_fork_1(t_phil *ph)
{
	if (sem_wait(ph->parms->forks))
		return (error("sem_wait\n"));
	if (sem_wait(ph->parms->prnt_sem))
		return (error("sem_wait\n"));
	printf("%zu %02zu has taken a fork\n", now_msec(), ph->num);
	if (sem_post(ph->parms->prnt_sem))
		return (error("sem_post\n"));
	return (1);
}
