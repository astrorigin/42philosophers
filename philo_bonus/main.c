/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/07 23:54:49 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/10 12:44:40 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static int	_initargs(t_parms *p, int ac, char *av[])
{
	if (ac < 5 || ac > 6)
		return (0);
	if (!ft_atosz_ex(av[1], &p->nphil, 1)
		|| !ft_atosz_ex(av[2], &p->ttdie, 0)
		|| !ft_atosz_ex(av[3], &p->tteat, 0)
		|| !ft_atosz_ex(av[4], &p->ttslp, 0))
		return (0);
	if (ac == 6 && !ft_atosz_ex(av[5], &p->nmeat, 0))
		return (0);
	return (1);
}

static int	_proceed(t_parms *p)
{
	size_t	i;

	i = -1;
	while (++i < p->nphil)
	{
		if (sem_wait(p->stop_sem))
			return (error("sem_wait\n"));
	}
	i = -1;
	while (++i < p->nphil)
		kill(p->pids[i], 9);
	return (1);
}

int	main(int argc, char *argv[])
{
	t_parms	p;
	size_t	i;

	memset(&p, 0, sizeof(t_parms));
	if (!_initargs(&p, argc, argv) || !parms_init(&p))
		return (1);
	i = -1;
	while (++i < p.nphil)
	{
		p.pids[i] = fork();
		if (p.pids[i] < 0)
			return (1 + error("fork\n"));
		if (p.pids[i] == 0)
			return (startproc(i, &p));
		if (i == 0 || (i % 2 == 0 && i != p.nphil - 1))
		{
			if (sem_post(p.phils[i]))
				return (1 + error("sem_post\n"));
		}
	}
	_proceed(&p);
	parms_fini(&p);
	return (0);
}
