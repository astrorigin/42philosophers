/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   phase_think.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 04:41:02 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/10 15:12:41 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	phase_think(t_phil *ph)
{
	if (sem_wait(ph->parms->prnt_sem))
		return (error("sem_wait\n"));
	printf("%zu %02zu is thinking\n", now_msec(), ph->num);
	if (sem_post(ph->parms->prnt_sem))
		return (error("sem_post\n"));
	if (sem_wait(ph->parms->phils[ph->num - 1]))
		return (error("sem_wait\n"));
	return (1);
}
