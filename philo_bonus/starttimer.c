/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   starttimer.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 03:53:46 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/10 15:14:25 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static void	_die(t_phil *ph)
{
	const size_t	n = now_msec();
	size_t			i;

	if (sem_wait(ph->parms->prnt_sem))
		error("sem_wait\n");
	printf("%zu %02zu died\n", n, ph->num);
	i = -1;
	while (++i < ph->parms->nphil)
	{
		if (sem_post(ph->parms->stop_sem))
			exit(1 + error("sem_post\n"));
	}
	exit(0);
}

static void	*_timer(void *param)
{
	t_phil	*ph;

	ph = param;
	while (1)
	{
		if (ph->flag)
			return (0);
		if (now_msec() - tv2msec(&ph->tv) > ph->parms->ttdie)
			_die(ph);
		usleep(500);
	}
	return (0);
}

int	starttimer(t_phil *ph)
{
	pthread_t	th;

	if (pthread_create(&th, 0, &_timer, ph))
		return (error("pthread_create\n"));
	if (pthread_detach(th))
		return (error("pthread_detach\n"));
	return (1);
}
