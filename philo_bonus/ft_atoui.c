/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoui.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/12 23:15:45 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/08 14:18:21 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

char	*ft_atosz(const char *s, size_t *ret)
{
	size_t	i;

	while (*s && ft_isspace(*s))
		++s;
	if (*s == '-')
		return (0);
	if (*s == '+')
		++s;
	if (!ft_isdigit(*s))
		return (0);
	i = 0;
	while (*s && ft_isdigit(*s))
	{
		i *= 10;
		i += *s - '0';
		++s;
	}
	*ret = i;
	return ((char *) s);
}
