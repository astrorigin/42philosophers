/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tv2msec.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 21:27:12 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/07 00:19:39 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

size_t	tv2msec(const t_st_tv *tv)
{
	return ((tv->tv_sec * 1000) + (tv->tv_usec / 1000));
}
