/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/04 20:52:11 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/10 15:06:05 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILO_H
# define PHILO_H

# include <assert.h>
# include <limits.h>
# include <fcntl.h>
# include <pthread.h>
# include <unistd.h>
# include <semaphore.h>
# include <signal.h>
# include <sys/time.h>
# include <stdio.h>
# include <stdlib.h>
# include <string.h>

# define SEMDIR	"pmarquis"

typedef struct s_parms
{
	size_t	nphil;
	size_t	ttdie;
	size_t	tteat;
	size_t	ttslp;
	size_t	nmeat;
	sem_t	*stop_sem;
	pid_t	*pids;
	sem_t	**phils;
	sem_t	*forks;
	sem_t	*prnt_sem;
}	t_parms;

typedef struct timeval	t_st_tv;

typedef struct s_phil
{
	size_t	num;
	t_st_tv	tv;
	size_t	nmeals;
	int		flag;
	t_parms	*parms;
}	t_phil;

int		error(const char *msg);
size_t	tv2msec(const t_st_tv *tv);
size_t	tv2usec(const t_st_tv *tv);

size_t	now_msec(void);
size_t	now_usec(void);
int		parms_init(t_parms *p);
int		parms_fini(t_parms *p);

size_t	next_(size_t fork, size_t nforks);
size_t	prev_(size_t fork, size_t nforks);

int		startproc(size_t i, t_parms *p);
int		starttimer(t_phil *ph);

int		phase_think(t_phil *ph);
int		phase_fork_1(t_phil *ph);
int		phase_fork_2(t_phil *ph);
int		phase_eat(t_phil *ph);
int		phase_sleep(t_phil *ph);

// libft

typedef unsigned long	t_ulong;

char	*ft_atosz(const char *s, size_t *ret);
int		ft_atosz_ex(const char *s, size_t *ret, int spec);
void	*ft_calloc(size_t nmemb, size_t size);
int		ft_isdigit(int c);
int		ft_isspace(int c);
char	*ft_itoa(int n, char ret[12]);
t_ulong	ft_ulmin(t_ulong a, t_ulong b);
int		ft_multovf(size_t a, size_t b);
int		ft_putstr(const char *s, int fd);
char	*ft_strcat(char *dest, const char *src);
char	*ft_strcpy(char *dst, const char *src);
char	*ft_strchr(const char *s, char c);
size_t	ft_strlen(const char *s);

#endif
