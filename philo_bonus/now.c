/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   now.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 16:17:24 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/05 21:29:25 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

size_t	now_msec(void)
{
	struct timeval	tv;

	gettimeofday(&tv, 0);
	return (tv2msec(&tv));
}

size_t	now_usec(void)
{
	struct timeval	tv;

	gettimeofday(&tv, 0);
	return (tv2usec(&tv));
}
