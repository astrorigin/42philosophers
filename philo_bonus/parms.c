/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parms.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 00:37:56 by pmarquis          #+#    #+#             */
/*   Updated: 2023/03/10 15:04:59 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

static sem_t	*my_sem_open(const char *name, unsigned int num)
{
	sem_t	*sem;

	sem = sem_open(name, O_CREAT, 00644, num);
	sem_unlink(name);
	return (sem);
}

static int	_phils_init(t_parms *p)
{
	char	buf[128];
	char	ib[12];
	int		i;

	i = -1;
	while (++i < (int)p->nphil)
	{
		ft_strcpy(buf, SEMDIR "phil");
		ft_strcat(buf, ft_itoa(i + 1, ib));
		p->phils[i] = my_sem_open(buf, 0);
		if (p->phils[i] == SEM_FAILED)
			return (error("sem_open\n"));
	}
	return (1);
}

int	parms_init(t_parms *p)
{
	p->stop_sem = my_sem_open(SEMDIR "parms", 0);
	if (p->stop_sem == SEM_FAILED)
		return (error("sem_open\n"));
	p->pids = ft_calloc(p->nphil, sizeof(pid_t));
	if (!p->pids)
		return (error("nomem\n"));
	p->phils = ft_calloc(p->nphil, sizeof(sem_t *));
	if (!p->phils)
		return (error("nomem\n"));
	if (!_phils_init(p))
		return (0);
	p->forks = my_sem_open(SEMDIR "forks", p->nphil);
	if (p->forks == SEM_FAILED)
		return (error("sem_open\n"));
	p->prnt_sem = my_sem_open(SEMDIR "prnt", 1);
	if (p->prnt_sem == SEM_FAILED)
		return (error("sem_open\n"));
	return (1);
}

static void	_phils_fini(t_parms *p)
{
	char	buf[128];
	char	ib[12];
	int		i;

	i = -1;
	while (++i < (int)p->nphil)
	{
		sem_close(p->phils[i]);
		ft_strcpy(buf, SEMDIR "phil");
		ft_strcat(buf, ft_itoa(i + 1, ib));
		sem_unlink(buf);
	}
}

int	parms_fini(t_parms *p)
{
	sem_close(p->stop_sem);
	sem_unlink(SEMDIR "parms");
	if (p->pids)
	{
		free(p->pids);
		p->pids = 0;
	}
	if (p->phils)
	{
		_phils_fini(p);
		free(p->phils);
		p->phils = 0;
	}
	sem_close(p->forks);
	sem_unlink(SEMDIR "forks");
	sem_close(p->prnt_sem);
	sem_unlink(SEMDIR "prnt");
	return (0);
}
