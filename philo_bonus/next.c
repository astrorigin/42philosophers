/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   next.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/05 03:18:39 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/07 00:14:57 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

size_t	next_(size_t fork, size_t nforks)
{
	if (fork == nforks - 1)
		fork = 0;
	else
		fork += 1;
	return (fork);
}

size_t	prev_(size_t fork, size_t nforks)
{
	if (fork == 0)
		fork = nforks - 1;
	else
		fork -= 1;
	return (fork);
}
