/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   startproc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: pmarquis <astrorigin@protonmail.com>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/08 03:35:02 by pmarquis          #+#    #+#             */
/*   Updated: 2023/01/08 14:34:58 by pmarquis         ###   lausanne.ch       */
/*                                                                            */
/* ************************************************************************** */

#include "philo.h"

int	startproc(size_t i, t_parms *p)
{
	t_phil	ph;

	memset(&ph, 0, sizeof(t_phil));
	ph.num = i + 1;
	gettimeofday(&ph.tv, 0);
	ph.parms = p;
	if (!starttimer(&ph))
		return (0);
	while (1)
	{
		if (!phase_think(&ph) || !phase_fork_1(&ph) || !phase_fork_2(&ph)
			|| !phase_eat(&ph) || !phase_sleep(&ph))
			break ;
	}
	ph.flag = 1;
	if (p->nmeat && p->nmeat == ph.nmeals)
	{
		parms_fini(p);
		return (0);
	}
	parms_fini(p);
	return (1);
}
